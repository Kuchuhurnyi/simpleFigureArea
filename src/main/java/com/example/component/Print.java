package com.example.component;

import com.example.component.figure.Figure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by Ежище on 11.04.2017.
 */
/** NB: либо при обращении к бину нужно использовать имя класса с маленькой буквы print, либо, если будет
 * использоваться иное имя, нужно писать @Component(value = "printer") */
@Component
public class Print {
    @Autowired
    @Qualifier("circ")
    private Figure figure;
//    Print(Figure figure) {
//        this.figure = figure;
//    }
    Print() {
        System.out.println("Bean print has been created");
    }
//    сеттер не нужен, т.к. это свойство теперь @Autowired
//    public void setFigure(Figure figure) {this.figure = figure;}

    public void printArea() {
        System.out.printf("%10s area = %.2f\n", figure.getName(), figure.area());
    }
}
