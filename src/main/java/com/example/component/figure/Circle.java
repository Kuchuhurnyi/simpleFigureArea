package com.example.component.figure;

import org.springframework.stereotype.Component;

/**
 * Created by Ежище on 11.04.2017.
 */
//@Component
public class Circle extends Figure{
    private double radius;
    Circle(String name, double radius) {
        super(name);
        this.radius = radius;
    }

    @Override
    public double area() {
        return Math.PI*Math.pow(radius, 2);
    }
}
