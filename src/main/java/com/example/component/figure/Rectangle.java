package com.example.component.figure;

import org.springframework.stereotype.Component;

/**
 * Created by Ежище on 11.04.2017.
 */
//@Component
public class Rectangle extends Figure {
    private double length, width;
    Rectangle(String name, double length, double width) {
        super(name);
        this.length = length;
        this.width = width;
    }

    @Override
    public double area() {
        return length * width;
    }
}
