package com.example.component.figure;

/**
 * Created by Ежище on 11.04.2017.
 */
public abstract class Figure {
    private String name;
    Figure(String name) {
        this.name = name;
        System.out.println("Bean " + name + " has been created");
    }
    public abstract double area();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
