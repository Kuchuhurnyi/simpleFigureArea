package com.example.execute;

import com.example.component.Print;
import com.example.component.figure.Circle;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
//@Configuration
//@ComponentScan(basePackages = {"com.example.component"}, basePackageClasses = Circle.class)
public class SimpleFigureAreaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleFigureAreaApplication.class, args);
				ApplicationContext ctx = new ClassPathXmlApplicationContext("context.xml");
//		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//		try {
//			ctx.register(SimpleFigureAreaApplication.class);
//			ctx.refresh();
//		} finally {
//			ctx.close();
//		}
		Print exec = (Print) ctx.getBean("print");
		exec.printArea();
	}
}
